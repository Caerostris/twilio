/*
 * Copyright (c) 2014 Keno Schwalb
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package twilio

import (
	"bytes"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

type Client struct {
	Sid   string
	Token string
}

type HandlerFunc func(text string) string

const xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<Response>\n"
const xmlFooter = "</Response>"

func (c Client) SendMessage(from string, to string, body string) error {
	// open session
	urlStr := "https://api.twilio.com/2010-04-01/Accounts/" + c.Sid + "/Messages.json"
	data := url.Values{}
	data.Set("From", from)
	data.Set("To", to)
	data.Set("Body", body)

	client := &http.Client{}
	r, _ := http.NewRequest("POST", urlStr, bytes.NewBufferString(data.Encode()))
	r.SetBasicAuth(c.Sid, c.Token)
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	resp, err := client.Do(r)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Sending message. Status: " + resp.Status)

	return err
}

func MessageHandler(handler HandlerFunc) http.HandlerFunc {
	function := func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()

		// process sms
		log.Println("Received message from " + query.Get("From") + ":" + query.Get("Body"))
		w.WriteHeader(200)
		w.Write([]byte(xmlHeader))
		w.Write([]byte(handler(query.Get("Body"))))
		w.Write([]byte(xmlFooter))
	}
	return function
}

func Message(msg string) string {
	return "	<Message>" + msg + "</Message>\n"
}
